﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour {

	public Transform container;
	public GameObject prefab;

	public int numThumbs = 1000;

	// TODO: Calculate these?
	public int numRows = 10;
	public int numColumns = 5;

	public RectTransform wrapper;

	private float _thumbnailHeight;
	private float _viewHeight;
	private int _currentRow = 0;

	private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();

	void Start () {
		createThumbnailVOList();
		createThumbnailPrefabs();
		GridLayoutGroup glg = container.GetComponent<GridLayoutGroup>();
		_thumbnailHeight = glg.cellSize.y + glg.spacing.y;
		_viewHeight = _thumbnailHeight * numRows - glg.spacing.y;
		wrapper.sizeDelta = new Vector2(0, (numThumbs / numColumns) * (_thumbnailHeight) - glg.spacing.y);
	}

	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		for (int i=0; i<numThumbs; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
			_thumbnailVOList.Add(thumbnailVO);
		}
	}

	private void createThumbnailPrefabs() {
		GameObject gameObj;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				gameObj = Instantiate(prefab);
				gameObj.transform.SetParent(container, false);
				gameObj.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[numColumns * i + j];
			}
		}
	}

	// OnScroll
	// Callback for the thumbnail scroll rect. Moves the top or bottom row of
	// thumbnails to create the illusion of infinite scrolling.
	public void OnScroll(Vector2 delta) {
		float newRow = -Mathf.Clamp(wrapper.anchoredPosition.y, 0, wrapper.sizeDelta.y - _viewHeight) / _thumbnailHeight;
		if (((int) newRow) < _currentRow) {
			_currentRow--;
			int nextThumbID = (Mathf.Abs(_currentRow) - 1) * numColumns + (numRows * numColumns);
			if (nextThumbID < _thumbnailVOList.Count - 1) {
				for (int i = 0; i < numColumns; i++) {
					Transform firstChild = container.GetChild(0);
					firstChild.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[nextThumbID++];
					firstChild.SetAsLastSibling();
				}
				container.localPosition = new Vector2(0, container.localPosition.y - (_thumbnailHeight));
			}
		} else if (((int) newRow) > _currentRow) {
			_currentRow++;
			int nextThumbID = (Mathf.Abs(_currentRow) + 1) * numColumns - 1;
			if (nextThumbID > 0) {
				for (int i = 0; i < numColumns; i++) {
					Transform lastChild = container.GetChild(container.childCount - 1);
					lastChild.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[nextThumbID--];
					lastChild.SetAsFirstSibling();
				}
				container.localPosition = new Vector2(0, container.localPosition.y + _thumbnailHeight);
			}
		}
	}
}
